﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


namespace MyPlugins
{
    [CustomEditor(typeof(Pathfinding))]
    public class PathfindingEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            Pathfinding myTarget = (Pathfinding)target;

            if (GUILayout.Button("Generate Maping"))
                myTarget.GenerateMaping();

            if (GUILayout.Button("ClearMap"))
                myTarget.Clear();

            if (GUILayout.Button("GeneratePath"))
                myTarget.GeneratePath();
        }

    }

}

